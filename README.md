# Brickshop - reference project

Example of Spring boot service.

> Spring boot, Reactor, Kafka, K6 performance tests, docker, monitoring.

Service represents virtual shop that sells bricks.

This example is used to show implementation of few basic use cases of some popular technologies/libraries that are used today for building microservices for cloud. e.g. REST API, reactive programing (Reactor), messaging (Kafka) and Docker.

REST api described by automatically generated [OpenAPI](doc/openapi/brickshop.yaml)

## How to run

There are three options how to run it.

1. All-in-one [docker-compose](docker-compose.yml)
2. separate [docker-compose files](docker/tools) for external dependencies/services
3. ~~install all external dependencies/services~~

### Prerequisites

- java 17 (or higher)
- docker
- ~~or installed PostgreSQL, Kafka, Prometheus and Grafana locally~~

### 1. Use of all-in-one docker-compose

1. navigate to root of the project where is located [docker-compose.yml](docker-compose.yml) file
2. run command `docker-compose up -d`
   + wait until it all starts
   + you can check it with `docker ps` command
3. open `http://localhost:8401/` in web browser or CURL command from terminal `curl http://localhost:8401/` to check if it is up. You should see `Welcome to Brickshop` page

### 2. Run from IDE with docker-compose for external services

1. run external services/dependencies - PostgreSQL and Kafka
   1. navigate to [folder with docker-compose files](docker/tools) `cd docker/tools`
   2. run `docker-compose up -d` inside each folder [postgres](docker/tools/postgres/docker-compose.yml) and [kafka](docker/tools/kafka/docker-compose.yml)
2. run Brickshop project from IDE or built jar file with `local` profile

## Using Brickshop service

Brickshop exposes REST API. It is available on localhost on port 8401 without any restriction (no security). Data are stored in relation DB ([How connect to DB](doc/Connections.md/#database---postgresql)) and in
Kafka ([How connect to Kafka](doc/Connections.md/#Kafka)).

### Basic use cases

1. create user
2. create item
3. create order
4. stock items in warehouse
5. automatic order processing depending on different types of messaging events (new order, item stocked)

For those basic use cases [Postman](doc/postman) collection is available in `doc/postman`

## Performance testing

For performance testing is used [k6](https://k6.io/) tool.

In k6 are tests configured with JS and can be run in many ways. In this example is used pure exe installation.

Three basic test scenarios are stored in [k6 folder](k6)

1. creation of a user [single creation of a user](k6/users.js)
2. creation of an order [single creation of an order](k6/order.js)
3. [complex scenario](k6/k6.js) where orders are coming with random number of items with random quantities. Also, items are stocked in random number. Orders are being fulfilled in relation to what is currently available in the warehouse
   and what is not.
   + complex scenario runs for 10 second with 10 virtual users creating orders at peek

### Run k6 performance tests

1. install k6 from https://k6.io/docs/get-started/installation/
2. start whole project either with [all-in-one docker-compose](#1-use-of-all-in-one-docker-compose) or [from IDE](#2-run-from-ide-with-docker-compose-for-external-services)
3. navigate to folder with [k6 scripts](k6) `cd k6`
4. run `k6 run <scriptName.js>` -> `k6 run users.js`

## Monitoring

Monitoring uses Grafana with Prometheus. Both have [docker-compose file](docker/tools/monitoring/docker-compose.yml).
To run it navigate to [monitoring folder](docker/tools/monitoring) and run `docker-compose up -d` command.

*Note: Monitoring is configured only to run with Brickshop service running from [IDE](#2-run-from-ide-with-docker-compose-for-external-services) (outside docker container). All-in-one is currently not set.*

*Tip: use k6 to populate data in Grafana. `k6.js` is suitable*

### Run Monitoring tools

1. run Grafana and Prometheus [docker-compose file](docker/tools/monitoring/docker-compose.yml)
2. configure Grafana
   1. go to `localhost:9090`
   2. login to Grafana with admin/admin
   3. click on plus sign on left panel in grafana and choose Import
   4. upload file [Dashboard for Grafana](doc/monitoring/Dashboard.json)
   5. [add data sources](doc/Connections.md/#set-data-sources)
3. now you should be able to see dashboard for Brickshop `Open orders` and `Closed orders`
3. run Brickshop service from IDE
4. *optional: populate data with postman or k6*

