import http from 'k6/http';
import {check, sleep} from 'k6';
import {randomString} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';

export const options = {
    vus: 3,
    duration: '5s',
};

function generateRandomNumber(maxNumber) {
    return Math.floor(Math.random() * maxNumber);
}

function prepareCreateUserPayload() {
    const firstName = "Perf-" + generateRandomNumber(500);
    const lastName = randomString(5) + generateRandomNumber(500);

    return JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: lastName + "@email.com",
        password: "password123"
    });
}

export default function () {
    const url = 'http://localhost:8401/users';
    const params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const res = http.post(url, prepareCreateUserPayload(), params);
    console.log(res.body);
    // check that response is 200
    check(res, {
        'response code was 200': (res) => res.status === 200,
    });

    sleep(1);
}


