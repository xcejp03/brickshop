import {uuidv4} from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';
import http from 'k6/http';
import {check, sleep} from 'k6';
import {randomString} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';


export const options = {
    vus: 3,
    duration: '2s',
};

function createOrder() {
    const url = 'http://localhost:8401/orders';
    const params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const customerId = uuidv4();
    const payload = JSON.stringify(
        {
            "items": {
                [uuidv4()]: 1,
                [uuidv4()]: 3,
                [uuidv4()]: 2,
            },
            "customerId": customerId,
            "description": randomString(20)
        }
    )

    const res = http.post(url, payload, params);
    console.log(res.body);
    // check that response is 200
    check(res, {
        'response code was 200': (res) => res.status === 200,
    });
    sleep(0.4);
}

export default function () {
    createOrder()
}


