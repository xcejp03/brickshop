import {uuidv4} from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';
import http from 'k6/http';
import {check, sleep} from 'k6';
import {randomString} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';

const params = {
    headers: {
        'Content-Type': 'application/json',
    },
};

export const options = {
    scenarios: {
        ItemsStorage: {
            executor: 'ramping-vus',
            exec: 'storeItems',
            stages: [
                {duration: '5s', target: 2},
                {duration: '4s', target: 10},
                {duration: '1s', target: 0},
            ]
        },
        OrdersCreation: {
            executor: 'ramping-vus',
            exec: 'createOrder',
            stages: [
                {duration: '3s', target: 3},
                {duration: '5s', target: 10},
                {duration: '2s', target: 0},
            ]
        }
    }
};

function generateRandomNumber(maxNumber) {
    return Math.floor(Math.random() * maxNumber);
}

export function setup() {
    const itemId1 = createItem();
    console.log("itemId1 - " + itemId1);
    const itemId2 = createItem();
    console.log("itemId2 - " + itemId2);
    const itemId3 = createItem();
    console.log("itemId3 - " + itemId3);
    return [itemId1, itemId2, itemId3];
}

function createItem() {
    const url = 'http://localhost:8401/items';

    const itemRequest = {
        "name": randomString(8),
        "description": randomString(18),
        "price": generateRandomNumber(300) + ".99"
    };
    const payload = JSON.stringify(itemRequest);

    const res = http.post(url, payload, params);
    console.log(res.body);

    check(res, {
        'response code was 200': (res) => res.status === 200,
    });

    const body = JSON.parse(res.body);
    return body.id;
}

export function createOrder(itemsAll) {
    const url = 'http://localhost:8401/orders';
    const params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    let itemsMap = new Map();
    itemsAll.forEach((element) => {
        if (Math.random() < 0.5) {
            itemsMap.set(element, generateRandomNumber(10))
        }
    })
    const items = {};
    itemsMap.forEach((value, key) => {
        items[key] = value
    })

    const payload = JSON.stringify(
        {
            "items": items,
            "customerId": uuidv4(),
            "description": randomString(12)
        }
    )

    const res = http.post(url, payload, params);
    console.log("Create order: " + res.body);
    check(res, {
        'response code was 200': (res) => res.status === 200,
    });
    sleep(1.9);

}

export function storeItems(itemsAll) {
    const url = 'http://localhost:8401/warehouse';

    let itemsMap = new Map();
    itemsAll.forEach((element) => {
        if (Math.random() < 0.5) {
            itemsMap.set(element, generateRandomNumber(10))
        }
    })
    const items = {};
    itemsMap.forEach((value, key) => {
        items[key] = value
    })
    const payload = JSON.stringify(items);
    const res = http.post(url, payload, params);
    console.log("Store items: " + res.body);
    check(res, {
        'response code was 204': (res) => res.status === 204,
    });
    sleep(1.9);
}


