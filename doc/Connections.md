# How to connect

## Database - PostgreSQL

```
Host: localhost
Port: 5432
User: brickshop
Password: brickshop
Database: brickshop
URL: jdbc:postgresql://localhost:5432/brickshop
```

## Kafka

```
Zookeeper Host: localhost
Zookeeper Port: 2181
bootstrap-servers: localhost:9092
```

## Prometheus

```
localhost:9090
login to grafana admin/admin
```

## Grafana

```
localhost:3000
login: admin/admin
```

### Set Data Sources

1. Open Configuration/Data Sources
2. Update default Prometheus settings
   ```
    Host: host.docker.internal:9090
   ```
3. Set DB connection - PostgreSQL
   ```
   Host: host.docker.internal:5432
   Database: brickshop
   User: brickshop
    ```
