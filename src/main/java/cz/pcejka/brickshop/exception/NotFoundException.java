package cz.pcejka.brickshop.exception;

public class NotFoundException extends BrickshopException {

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }
}
