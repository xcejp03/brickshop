package cz.pcejka.brickshop.exception;

public class BrickshopException extends RuntimeException {

    public BrickshopException() {
    }

    public BrickshopException(String message) {
        super(message);
    }

    public BrickshopException(String message, Throwable cause) {
        super(message, cause);
    }

    public BrickshopException(Throwable cause) {
        super(cause);
    }

    public BrickshopException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
