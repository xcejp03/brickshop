package cz.pcejka.brickshop.messaging.stream;

import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.messaging.model.Event;
import cz.pcejka.brickshop.messaging.model.evaluation.*;
import cz.pcejka.brickshop.messaging.model.order.CreateOrderEvent;
import cz.pcejka.brickshop.messaging.model.order.DeleteOrderEvent;
import cz.pcejka.brickshop.messaging.model.order.Order;
import cz.pcejka.brickshop.messaging.model.order.UpdateOrderEvent;
import cz.pcejka.brickshop.services.MetricService;
import cz.pcejka.brickshop.services.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static cz.pcejka.brickshop.domain.Order.State.CLOSED;
import static cz.pcejka.brickshop.messaging.stream.OrderStreamConfiguration.STORE_NAME;

@Slf4j
@RequiredArgsConstructor
public class OrderViewStoreTransformer implements Transformer<String, Event, KeyValue<String, Event>>, Event.EventVisitor {

    private final DomainMapper domainMapper;
    private final OrderService orderService;
    private final MetricService metricService;
    private KeyValueStore<String, Order> keyValueStore;
    private ProcessorContext processorContext;

    @Override
    public void init(ProcessorContext context) {
        processorContext = context;
        keyValueStore = context.getStateStore(STORE_NAME);
    }

    @Override
    public KeyValue<String, Event> transform(String key, Event event) {
        var res = event.processEvent(key, this).block();
        return KeyValue.pair(key, res);
    }

    @Override
    public void close() {
    }

    @Override
    public Mono<Event> handle(String key, CreateOrderEvent event) {
        Order order = domainMapper.toOrder(event, key);
        keyValueStore.put(key, order);
        log.info("Into store '{}' was put order '{}' with value '{}'", STORE_NAME, key, order);
        return Mono.just(ProcessOrderEvent.builder().source(AbstractEvaluationEvent.EventSource.ORDER_CREATE).build());
    }

    @Override
    public Mono<Event> handle(String key, UpdateOrderEvent event) {
        Order originOrder = keyValueStore.get(key);
        Order updatedOrder = domainMapper.updateOrder(event, originOrder);
        keyValueStore.put(key, updatedOrder);
        log.info("In store '{}' was updated order '{}' from '{}' to '{}'", STORE_NAME, key, originOrder, updatedOrder);
        return Mono.just(ProcessOrderEvent.builder().source(AbstractEvaluationEvent.EventSource.ORDER_UPDATE).build());
    }

    @Override
    public Mono<Event> handle(String key, DeleteOrderEvent event) {
        keyValueStore.delete(key);
        log.info("From store '{}' was deleted order '{}'", STORE_NAME, key);
        return Mono.just(ProcessOrderEvent.builder().source(AbstractEvaluationEvent.EventSource.ORDER_DELETE).build());
    }

    @Override
    public Mono<Event> handle(String key, RefreshProcessingEvent event) {
        log.info("Received refresh processing for order '{}'", key);
        return fulfillOrder(key)
                .map(uuid -> new FulfillProcessingEvent(List.of(uuid)));
    }

    @Override
    public Mono<Event> handle(String key, FulfillProcessingEvent event) {
        return Flux.fromIterable(event.getOrders())
                .flatMap(uuid -> orderService.updateOrderState(uuid, CLOSED)
                        .doOnSuccess(uuid1 -> {
                            metricService.incrementClosedOrder();
                            log.info("Orders '{}' has been fulfilled", event.getOrders());
                        }))
                .collectList()
                .thenReturn(event)
                .flatMap(fulfillProcessingEvent -> Mono.empty());
    }

    @Override
    public Mono<Event> handle(String key, ProcessOrderEvent event) {
        log.info("Received ProcessOrderEvent change event for order '{}'", key);
        return fulfillOrder(key)
                .map(uuid -> new FulfillProcessingEvent(List.of(uuid)));
    }

    @Override
    public Mono<Event> handle(String key, WarehouseChangeEvent event) {
        var ordersIterator = keyValueStore.all();
        Map<String, Order> storeOrders = new HashMap<>();
        ordersIterator.forEachRemaining(stringOrderKeyValue -> storeOrders.put(stringOrderKeyValue.key, stringOrderKeyValue.value));
        return Flux.fromIterable(storeOrders.entrySet())
                .flatMap(entry -> fulfillOrder(entry.getKey(), entry.getValue()))
                .collectList()
                .map(FulfillProcessingEvent::new);
    }

    private Mono<UUID> fulfillOrder(String key) {
        var orderFromStore = keyValueStore.get(key);
        return fulfillOrder(key, orderFromStore);
    }

    private Mono<UUID> fulfillOrder(String key, Order orderFromStore) {
        if (orderFromStore == null || key == null) {
            return Mono.empty();
        }

        return orderService.processOrderStream(orderFromStore)
                .flatMap(newOrder -> saveIfDiffer(key, orderFromStore, newOrder))
                .flatMap(order -> {
                    if (isOrderFulfilled(order)) {
                        keyValueStore.delete(key);
                        return Mono.just(UUID.fromString(key));
                    } else {
                        return Mono.empty();
                    }
                });
    }

    private boolean isOrderFulfilled(Order order) {
        return order.getItems().values().stream().noneMatch(entry -> entry.getValue().equals(false));
    }

    private Mono<Order> saveIfDiffer(String key, Order oldOrder, Order newOrder) {
        if (!oldOrder.equals(newOrder)) {
            return Mono.defer(() -> {
                keyValueStore.put(key, newOrder);
                return Mono.just(newOrder);
            });
        } else {
            return Mono.just(newOrder);
        }
    }

}
