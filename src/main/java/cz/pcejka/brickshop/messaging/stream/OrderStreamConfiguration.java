package cz.pcejka.brickshop.messaging.stream;

import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.messaging.model.Event;
import cz.pcejka.brickshop.messaging.model.order.Order;
import cz.pcejka.brickshop.services.MetricService;
import cz.pcejka.brickshop.services.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import java.util.function.Function;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.serializer.JsonSerde;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class OrderStreamConfiguration {

    public static final String STORE_NAME = "order-view";

    private final DomainMapper domainMapper;
    private final OrderService orderService;
    private final MetricService metricService;

    @Bean
    public Function<KStream<String, Event>, KStream<String, Event>> processOrderEvents() {
        return stringStringKStream -> stringStringKStream
                .peek((s, s2) -> log.info("Input value in stream processOrderEvents: {} - {}", s, s2))
                .transform(() -> new OrderViewStoreTransformer(domainMapper, orderService, metricService), STORE_NAME)
                .filter((sn, event) -> event != null)
                .peek((key, value) -> log.info("Output msg from stream processOrderEvents '{}'", value));
    }

    @Bean
    public StoreBuilder<KeyValueStore<String, Order>> orderView() {
        return Stores.keyValueStoreBuilder(Stores.persistentKeyValueStore(STORE_NAME), Serdes.String(), new JsonSerde<>(Order.class))
                .withCachingEnabled();
    }
}
