package cz.pcejka.brickshop.messaging.model.evaluation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEvaluationEvent {

    private EventSource source;

    public enum EventSource {
        ORDER_CREATE, ORDER_UPDATE, ORDER_DELETE, ITEMS_STOCKED
    }
}
