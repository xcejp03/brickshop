package cz.pcejka.brickshop.messaging.model.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.Map;
import java.util.UUID;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractOrderEvent {

    private Map<UUID, Long> items;

    @NotNull
    private UUID customerId;

    @NotBlank
    private String description;

}
