package cz.pcejka.brickshop.messaging.model.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.pcejka.brickshop.messaging.model.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import reactor.core.publisher.Mono;

import static cz.pcejka.brickshop.messaging.model.order.UpdateOrderEvent.UPDATE_TYPE;

@SuperBuilder
@Jacksonized
@RequiredArgsConstructor
@ToString(callSuper = true)
@JsonTypeName(UPDATE_TYPE)
@Getter
public class UpdateOrderEvent implements Event {

    public static final String UPDATE_TYPE = "UPDATE";

    private final String description;

    @Override
    public Mono<Event> processEvent(String key, EventVisitor visitor) {
        return visitor.handle(key, this);
    }
}
