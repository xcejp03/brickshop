package cz.pcejka.brickshop.messaging.model.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.pcejka.brickshop.messaging.model.Event;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import reactor.core.publisher.Mono;

import static cz.pcejka.brickshop.messaging.model.order.CreateOrderEvent.CREATE_TYPE;

@SuperBuilder
@Jacksonized
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(CREATE_TYPE)
public class CreateOrderEvent extends AbstractOrderEvent implements Event {

    public static final String CREATE_TYPE = "CREATE";

    @Override
    public Mono<Event> processEvent(String key, EventVisitor visitor) {
        return visitor.handle(key, this);
    }
}
