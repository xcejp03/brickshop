package cz.pcejka.brickshop.messaging.model.evaluation;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.pcejka.brickshop.messaging.model.Event;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import reactor.core.publisher.Mono;

import static cz.pcejka.brickshop.messaging.model.evaluation.RefreshProcessingEvent.REFRESH_TYPE;

@SuperBuilder
@Jacksonized
@NoArgsConstructor
@ToString(callSuper = true)
@JsonTypeName(REFRESH_TYPE)
public class RefreshProcessingEvent implements Event {

    public static final String REFRESH_TYPE = "REFRESH";

    @Override
    public Mono<Event> processEvent(String key, EventVisitor visitor) {
        return visitor.handle(key, this);
    }
}
