package cz.pcejka.brickshop.messaging.model.evaluation;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.pcejka.brickshop.messaging.model.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static cz.pcejka.brickshop.messaging.model.evaluation.FulfillProcessingEvent.FULFILL_TYPE;

@SuperBuilder
@Jacksonized
@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString(callSuper = true)
@JsonTypeName(FULFILL_TYPE)
public class FulfillProcessingEvent implements Event {

    public static final String FULFILL_TYPE = "FULFILLED";

    private List<UUID> orders = new ArrayList<>();

    @Override
    public Mono<Event> processEvent(String key, EventVisitor visitor) {
        return visitor.handle(key, this);
    }
}
