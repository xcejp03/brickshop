package cz.pcejka.brickshop.messaging.model.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.pcejka.brickshop.messaging.model.Event;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import reactor.core.publisher.Mono;

@Getter
@Builder(toBuilder = true)
@Jacksonized
@NoArgsConstructor
@JsonTypeName(DeleteOrderEvent.DELETE_TYPE)
public class DeleteOrderEvent implements Event {

    public static final String DELETE_TYPE = "DELETE";

    @Override
    public Mono<Event> processEvent(String key, EventVisitor visitor) {
        return visitor.handle(key, this);
    }
}
