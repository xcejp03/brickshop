package cz.pcejka.brickshop.messaging.model.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Data
@Builder(toBuilder = true)
@FieldNameConstants(asEnum = true)
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    private UUID id;

    private UUID customerId;

    private String description;

    private Map<UUID, Map.Entry<Long, Boolean>> items;

    @CreatedDate
    private Instant created = Instant.now();

    @LastModifiedDate
    private Instant updated;

}
