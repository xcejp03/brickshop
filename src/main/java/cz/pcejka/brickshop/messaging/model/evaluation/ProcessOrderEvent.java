package cz.pcejka.brickshop.messaging.model.evaluation;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.pcejka.brickshop.messaging.model.Event;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import reactor.core.publisher.Mono;

import static cz.pcejka.brickshop.messaging.model.evaluation.ProcessOrderEvent.PROCESS_TYPE;

@SuperBuilder
@Jacksonized
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(PROCESS_TYPE)
public class ProcessOrderEvent extends AbstractEvaluationEvent implements Event {

    public static final String PROCESS_TYPE = "PROCESS";

    @Override
    public Mono<Event> processEvent(String key, EventVisitor visitor) {
        return visitor.handle(key, this);
    }
}
