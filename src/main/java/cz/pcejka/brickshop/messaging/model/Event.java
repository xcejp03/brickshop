package cz.pcejka.brickshop.messaging.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.pcejka.brickshop.messaging.model.evaluation.FulfillProcessingEvent;
import cz.pcejka.brickshop.messaging.model.evaluation.ProcessOrderEvent;
import cz.pcejka.brickshop.messaging.model.evaluation.RefreshProcessingEvent;
import cz.pcejka.brickshop.messaging.model.evaluation.WarehouseChangeEvent;
import cz.pcejka.brickshop.messaging.model.order.CreateOrderEvent;
import cz.pcejka.brickshop.messaging.model.order.DeleteOrderEvent;
import cz.pcejka.brickshop.messaging.model.order.UpdateOrderEvent;
import reactor.core.publisher.Mono;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", defaultImpl = VoidOrderEvent.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreateOrderEvent.class),
        @JsonSubTypes.Type(value = UpdateOrderEvent.class),
        @JsonSubTypes.Type(value = DeleteOrderEvent.class),
        @JsonSubTypes.Type(value = RefreshProcessingEvent.class),
        @JsonSubTypes.Type(value = ProcessOrderEvent.class),
        @JsonSubTypes.Type(value = FulfillProcessingEvent.class),
        @JsonSubTypes.Type(value = WarehouseChangeEvent.class)
})
public interface Event {

    Mono<Event> processEvent(String key, EventVisitor visitor);

    interface EventVisitor {

        Mono<Event> handle(String key, CreateOrderEvent event);

        Mono<Event> handle(String key, UpdateOrderEvent event);

        Mono<Event> handle(String key, DeleteOrderEvent event);

        Mono<Event> handle(String key, RefreshProcessingEvent event);

        Mono<Event> handle(String key, FulfillProcessingEvent event);

        Mono<Event> handle(String key, ProcessOrderEvent event);

        Mono<Event> handle(String key, WarehouseChangeEvent event);

    }

}
