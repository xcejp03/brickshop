package cz.pcejka.brickshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.r2dbc.config.EnableR2dbcAuditing;

@EnableR2dbcAuditing
@SpringBootApplication
@EnableConfigurationProperties
public class BrickshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrickshopApplication.class, args);
    }

}
