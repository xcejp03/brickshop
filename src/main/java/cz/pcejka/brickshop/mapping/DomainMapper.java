package cz.pcejka.brickshop.mapping;

import cz.pcejka.brickshop.domain.Item;
import cz.pcejka.brickshop.domain.Order;
import cz.pcejka.brickshop.domain.User;
import cz.pcejka.brickshop.messaging.model.order.CreateOrderEvent;
import cz.pcejka.brickshop.messaging.model.order.UpdateOrderEvent;
import cz.pcejka.brickshop.model.*;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.Map;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface DomainMapper {

    @Mapping(target = "password", expression = "java(org.springframework.security.crypto.bcrypt.BCrypt.hashpw(createUserDTO.getPassword(),org.springframework.security.crypto.bcrypt.BCrypt.gensalt()))")
    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    User toDomain(CreateUserDto createUserDto);

    @Mapping(target = "updated", ignore = true)
    @Mapping(target = "newlyCreated", constant = "false")
    User updateUser(UpdateUserDto updateUserDto, @MappingTarget User user);

    UserDto fromDomain(User user);

    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    Item toDomain(CreateItemDto createItemDto);

    @Mapping(target = "updated", ignore = true)
    @Mapping(target = "newlyCreated", constant = "false")
    Item updateItem(UpdateItemDto updateItemDto, @MappingTarget Item item);

    ItemDto fromDomain(Item item);

    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    Order toDomain(CreateOrderDto createOrderDto);

    @Mapping(target = "updated", ignore = true)
    @Mapping(target = "newlyCreated", constant = "false")
    Order updateOrder(UpdateOrderDto updateOrderDto, @MappingTarget Order order);

    @Mapping(target = "items", ignore = true)
    OrderDto fromDomain(Order order);

    @AfterMapping
    default void setItems(CreateOrderEvent orderEvent, @MappingTarget cz.pcejka.brickshop.messaging.model.order.Order.OrderBuilder order) {
        var items = orderEvent.getItems().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> Map.entry(entry.getValue(), false)));
        order.items(items);
    }

    @Mapping(target = "created", expression = "java(java.time.Instant.now())")
    @Mapping(target = "id", expression = "java(java.util.UUID.fromString(id))")
    @Mapping(target = "items", ignore = true)
    cz.pcejka.brickshop.messaging.model.order.Order toOrder(CreateOrderEvent event, String id);

    CreateOrderEvent toEvent(CreateOrderDto createOrderDto);

    @Mapping(target = "updated", expression = "java(java.time.Instant.now())")
    cz.pcejka.brickshop.messaging.model.order.Order updateOrder(UpdateOrderEvent event, @MappingTarget cz.pcejka.brickshop.messaging.model.order.Order order);

}
