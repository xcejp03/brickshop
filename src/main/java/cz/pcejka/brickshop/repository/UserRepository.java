package cz.pcejka.brickshop.repository;

import cz.pcejka.brickshop.domain.User;

import java.util.UUID;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface UserRepository extends ReactiveCrudRepository<User, UUID> {

}
