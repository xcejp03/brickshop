package cz.pcejka.brickshop.repository;

import cz.pcejka.brickshop.domain.OrderOrderContent;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface OrderOrderContentRepository extends ReactiveCrudRepository<OrderOrderContent, Void>, OrderOrderContentRepositoryCustom {

}
