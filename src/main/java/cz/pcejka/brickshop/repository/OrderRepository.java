package cz.pcejka.brickshop.repository;

import cz.pcejka.brickshop.domain.Order;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface OrderRepository extends ReactiveCrudRepository<Order, UUID> {

    Mono<Long> countAllByState(Order.State state);

}
