package cz.pcejka.brickshop.repository;

import cz.pcejka.brickshop.domain.OrderOrderContent;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class OrderOrderContentRepositoryCustomImpl implements OrderOrderContentRepositoryCustom {

    private final DatabaseClient client;

    @Override
    public Mono<Void> insert(OrderOrderContent content) {
        return client.sql("INSERT INTO order_ordercontent (order_id, content_id) values (:orderId, :contentId)")
                .bind("orderId", content.getOrderId())
                .bind("contentId", content.getContentId())
                .then();
    }
}
