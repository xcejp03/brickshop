package cz.pcejka.brickshop.repository;

import cz.pcejka.brickshop.domain.OrderContent;

import java.util.UUID;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface OrdercontentRepository extends ReactiveCrudRepository<OrderContent, UUID> {

}
