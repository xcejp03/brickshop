package cz.pcejka.brickshop.repository;

import cz.pcejka.brickshop.domain.OrderOrderContent;
import reactor.core.publisher.Mono;

public interface OrderOrderContentRepositoryCustom {

    Mono<Void> insert(OrderOrderContent content);
}
