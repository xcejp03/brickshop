package cz.pcejka.brickshop.repository;

import cz.pcejka.brickshop.domain.Item;
import reactor.core.publisher.Mono;

import java.util.UUID;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ItemRepository extends ReactiveCrudRepository<Item, UUID> {

    Mono<Boolean> existsByIdAndQuantityGreaterThanEqual(UUID itemId, Long quantity);
}
