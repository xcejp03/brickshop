package cz.pcejka.brickshop.controller;

import cz.pcejka.brickshop.exception.NotFoundException;
import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.model.CreateUserDto;
import cz.pcejka.brickshop.model.UpdateUserDto;
import cz.pcejka.brickshop.model.UserDto;
import cz.pcejka.brickshop.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.UUID;

import static cz.pcejka.brickshop.controller.UserController.USERS;

@Tag(name = "User operations", description = "User related operations")
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(USERS)
public class UserController {

    public static final String USERS = "/users";

    private final UserService userService;
    private final DomainMapper domainMapper;

    @Operation(summary = "Get all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of all users",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))})})
    @GetMapping
    @ResponseBody
    public Flux<UserDto> getAllUsers() {
        return userService.getUsers()
                .map(domainMapper::fromDomain)
                .doOnSubscribe(subscription -> log.debug("Get all users"));
    }

    @Operation(summary = "Get user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)})
    @GetMapping("/{userId}")
    public Mono<ResponseEntity<UserDto>> getUser(@PathVariable UUID userId) {
        return userService.getUser(userId)
                .map(user -> ResponseEntity.ok(domainMapper.fromDomain(user)))
                .switchIfEmpty(Mono.error(new NotFoundException("User '" + userId + "' not found")))
                .doOnSubscribe(subscription -> log.debug("Get user by id '{}'", userId));
    }

    @Operation(summary = "Create user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content)})
    @PostMapping
    public Mono<ResponseEntity<UserDto>> createUser(@Valid @RequestBody CreateUserDto createUserDTO) {
        log.info("CreateUser - '{}'", createUserDTO);
        return userService.createUser(createUserDTO)
                .map(user -> new ResponseEntity<>(domainMapper.fromDomain(user), HttpStatus.CREATED))
                .doOnSubscribe(subscription -> log.debug("Create user"));
    }

    @Operation(summary = "Update user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)})
    @PutMapping("/{userId}")
    public Mono<ResponseEntity<UserDto>> updateUser(@PathVariable UUID userId, @Valid @RequestBody UpdateUserDto updateUserDTO) {
        return userService.updateUser(userId, updateUserDTO)
                .map(user -> ResponseEntity.ok(domainMapper.fromDomain(user)))
                .doOnSubscribe(subscription -> log.debug("Update user '{}'", userId));
    }

    @Operation(summary = "Delete user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User deleted",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema())}),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)})
    @DeleteMapping("/{userId}")
    public Mono<ResponseEntity<Void>> deleteUser(@PathVariable UUID userId) {
        return userService.delete(userId)
                .doOnSubscribe(subscription -> log.debug("Delete user '{}'", userId))
                .thenReturn(ResponseEntity.noContent().build());
    }

}
