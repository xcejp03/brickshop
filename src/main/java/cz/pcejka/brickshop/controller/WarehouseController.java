package cz.pcejka.brickshop.controller;

import cz.pcejka.brickshop.services.ItemService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(WarehouseController.WAREHOUSE)
public class WarehouseController {

    public static final String WAREHOUSE = "/warehouse";

    private final ItemService itemService;

    @PostMapping
    public Mono<ResponseEntity<Void>> toStore(@RequestBody Map<UUID, Long> items) {
        return itemService.storeItems(items)
                .thenReturn(ResponseEntity.noContent().build());
    }

    @PostMapping("/quantity")
    public Flux<Map.Entry<UUID, Long>> getItemsQuantity(@RequestBody List<UUID> items) {
        return itemService.getItemsQuantity(items);
    }
}
