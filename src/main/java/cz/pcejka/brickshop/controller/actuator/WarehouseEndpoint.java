/*
 * Copyright (C) Lundegaard a.s. 2022 - All Rights Reserved
 *
 * Proprietary and confidential. Unauthorized copying of this file, via any
 * medium is strictly prohibited.
 */
package cz.pcejka.brickshop.controller.actuator;

import cz.pcejka.brickshop.services.ItemService;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
@AllArgsConstructor
@RestControllerEndpoint(id = "warehouse")
public class WarehouseEndpoint {

    private final ItemService itemService;

    @PutMapping("/itemsamount")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> setItemsAmount(@RequestBody Map<UUID, Long> items) {
        return itemService.setItemAmount(items);
    }

    @GetMapping("/stock")
    @ResponseStatus(HttpStatus.OK)
    public Flux<Map.Entry<UUID, Long>> getAllItems() {
        return itemService.getAllItemsQuantity();
    }
}
