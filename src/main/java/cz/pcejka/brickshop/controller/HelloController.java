package cz.pcejka.brickshop.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Hello controller", description = "Welcome page. Root URL of Brickshop service.")
@RestController
public class HelloController {

    @Operation(summary = "Returns welcome string.")
    @GetMapping("/")
    public String index() {
        return "Welcome to Brickshop";
    }
}
