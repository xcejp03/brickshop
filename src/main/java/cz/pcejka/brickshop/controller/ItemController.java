package cz.pcejka.brickshop.controller;

import cz.pcejka.brickshop.exception.NotFoundException;
import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.model.CreateItemDto;
import cz.pcejka.brickshop.model.ItemDto;
import cz.pcejka.brickshop.model.UpdateItemDto;
import cz.pcejka.brickshop.services.ItemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static cz.pcejka.brickshop.controller.ItemController.ITEMS;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(ITEMS)
public class ItemController {

    public static final String ITEMS = "/items";

    private final ItemService itemService;
    private final DomainMapper domainMapper;

    @GetMapping
    @ResponseBody
    public Flux<ItemDto> getAllItems() {
        return itemService.getItems()
                .map(domainMapper::fromDomain)
                .doOnSubscribe(subscription -> log.debug("Get all items"));
    }

    @GetMapping("/{itemId}")
    public Mono<ResponseEntity<ItemDto>> getItem(@PathVariable UUID itemId) {
        return itemService.getItem(itemId)
                .map(item -> ResponseEntity.ok(domainMapper.fromDomain(item)))
                .switchIfEmpty(Mono.error(new NotFoundException("Item '" + itemId + "' not found")))
                .doOnSubscribe(subscription -> log.debug("Get item by id '{}'", itemId));
    }

    @PostMapping
    public Mono<ResponseEntity<ItemDto>> createItem(@Valid @RequestBody CreateItemDto createItemDTO) {
        return itemService.createItem(createItemDTO)
                .map(item -> ResponseEntity.ok(domainMapper.fromDomain(item)))
                .doOnSubscribe(subscription -> log.debug("Create item"));
    }

    @PutMapping("/{itemId}")
    public Mono<ResponseEntity<ItemDto>> updateItem(@PathVariable UUID itemId, @Valid @RequestBody UpdateItemDto updateItemDTO) {
        return itemService.updateItem(itemId, updateItemDTO)
                .map(item -> ResponseEntity.ok(domainMapper.fromDomain(item)))
                .doOnSubscribe(subscription -> log.debug("Update item '{}'", itemId));
    }

    @DeleteMapping("/{itemId}")
    public Mono<ResponseEntity<Void>> deleteItem(@PathVariable UUID itemId) {
        return itemService.deleteItem(itemId)
                .doOnSubscribe(subscription -> log.debug("Delete item '{}'", itemId))
                .thenReturn(ResponseEntity.noContent().build());
    }

}
