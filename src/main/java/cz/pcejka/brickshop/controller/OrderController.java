package cz.pcejka.brickshop.controller;

import cz.pcejka.brickshop.exception.NotFoundException;
import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.model.CreateOrderDto;
import cz.pcejka.brickshop.model.OrderDto;
import cz.pcejka.brickshop.model.UpdateOrderDto;
import cz.pcejka.brickshop.services.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static cz.pcejka.brickshop.controller.OrderController.ORDERS;

@Slf4j
@Validated
@RequiredArgsConstructor
@Controller
@RequestMapping(ORDERS)
public class OrderController {

    public static final String ORDERS = "/orders";

    private final OrderService orderService;
    private final DomainMapper domainMapper;

    @GetMapping
    @ResponseBody
    public Flux<OrderDto> getAllOrders() {
        return orderService.getOrders()
                .map(domainMapper::fromDomain)
                .doOnSubscribe(subscription -> log.debug("Get all Orders"));
    }

    @GetMapping("/{orderId}")
    public Mono<ResponseEntity<OrderDto>> getOrder(@PathVariable UUID orderId) {
        return orderService.getOrder(orderId)
                .map(order -> ResponseEntity.ok(domainMapper.fromDomain(order)))
                .switchIfEmpty(Mono.error(new NotFoundException("Order '" + orderId + "' not found")))
                .doOnSubscribe(subscription -> log.debug("Get Order by id '{}'", orderId));
    }

    @PostMapping
    public Mono<ResponseEntity<OrderDto>> createOrder(@RequestBody @Valid CreateOrderDto createOrderDTO) {
        log.info("CreateOrder - '{}'", createOrderDTO);
        return orderService.createOrder(createOrderDTO)
                .map(order -> ResponseEntity.ok(domainMapper.fromDomain(order)))
                .doOnSubscribe(subscription -> log.debug("Create Order"));
    }

    @PutMapping("/{orderId}")
    public Mono<ResponseEntity<OrderDto>> updateOrder(@PathVariable UUID orderId, @RequestBody UpdateOrderDto updateOrderDTO) {
        return orderService.updateOrder(orderId, updateOrderDTO)
                .map(order -> ResponseEntity.ok(domainMapper.fromDomain(order)))
                .doOnSubscribe(subscription -> log.debug("Update Order '{}'", orderId));
    }

    @DeleteMapping("/{orderId}")
    public Mono<ResponseEntity<Void>> deleteOrder(@PathVariable UUID orderId) {
        return orderService.deleteOrder(orderId)
                .doOnSubscribe(subscription -> log.debug("Delete Order '{}'", orderId))
                .thenReturn(ResponseEntity.noContent().build());
    }

    @PostMapping("/{orderId}/refresh")
    public Mono<ResponseEntity<Void>> refreshOrder(@PathVariable UUID orderId) {
        log.info("refreshOrder - '{}'", orderId);
        return orderService.refreshOrder(orderId)
                .doOnSubscribe(subscription -> log.debug("Refresh Order"))
                .thenReturn(ResponseEntity.noContent().build());
    }

}
