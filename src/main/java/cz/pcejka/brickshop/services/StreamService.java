package cz.pcejka.brickshop.services;

import cz.pcejka.brickshop.messaging.model.Event;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface StreamService {

    Mono<Void> sendOrderEvent(UUID key, Event payload);

    Mono<Void> sendOrderEvaluation(UUID key, Event payload);

}
