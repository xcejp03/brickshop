package cz.pcejka.brickshop.services;

import cz.pcejka.brickshop.domain.Item;
import cz.pcejka.brickshop.model.CreateItemDto;
import cz.pcejka.brickshop.model.UpdateItemDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ItemService {

    Mono<Item> getItem(UUID id);

    Flux<Item> getItems();

    Mono<Item> createItem(CreateItemDto createItemDto);

    Mono<Item> updateItem(UUID id, UpdateItemDto updateItemDto);

    Mono<Void> deleteItem(UUID id);

    Mono<Void> storeItems(Map<UUID, Long> items);

    Flux<Map.Entry<UUID, Long>> getItemsQuantity(List<UUID> items);

    Mono<Void> setItemAmount(Map<UUID, Long> items);

    Flux<Map.Entry<UUID, Long>> getAllItemsQuantity();
}
