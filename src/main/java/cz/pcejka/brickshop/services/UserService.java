package cz.pcejka.brickshop.services;

import cz.pcejka.brickshop.domain.User;
import cz.pcejka.brickshop.model.CreateUserDto;
import cz.pcejka.brickshop.model.UpdateUserDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface UserService {

    Mono<User> getUser(UUID id);

    Flux<User> getUsers();

    Mono<User> createUser(CreateUserDto createUserDTO);

    Mono<User> updateUser(UUID id, UpdateUserDto updateUserDTO);

    Mono<Void> delete(UUID id);
}
