package cz.pcejka.brickshop.services;

import cz.pcejka.brickshop.domain.Order;
import cz.pcejka.brickshop.model.CreateOrderDto;
import cz.pcejka.brickshop.model.UpdateOrderDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface OrderService {

    Mono<Order> getOrder(UUID id);

    Flux<Order> getOrders();

    Mono<Order> createOrder(CreateOrderDto createOrderDTO);

    Mono<cz.pcejka.brickshop.messaging.model.order.Order> processOrderStream(
            cz.pcejka.brickshop.messaging.model.order.Order order);

    Mono<Order> updateOrder(UUID id, UpdateOrderDto updateOrderDTO);

    Mono<Void> deleteOrder(UUID id);

    Mono<Void> refreshOrder(UUID orderId);

    Mono<UUID> updateOrderState(UUID orderId, Order.State state);
}
