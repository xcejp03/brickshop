package cz.pcejka.brickshop.services;

public interface MetricService {

    String OPEN_ORDER_GAUGE = "brickshop.open.order.gauge";
    String CLOSED_ORDER_COUNTER = "brickshop.closed.order.counter";

    void incrementClosedOrder();
}
