package cz.pcejka.brickshop.services.impl;

import cz.pcejka.brickshop.domain.Order;
import cz.pcejka.brickshop.domain.OrderContent;
import cz.pcejka.brickshop.domain.OrderOrderContent;
import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.messaging.model.evaluation.RefreshProcessingEvent;
import cz.pcejka.brickshop.messaging.model.order.DeleteOrderEvent;
import cz.pcejka.brickshop.messaging.model.order.UpdateOrderEvent;
import cz.pcejka.brickshop.model.CreateOrderDto;
import cz.pcejka.brickshop.model.UpdateOrderDto;
import cz.pcejka.brickshop.repository.ItemRepository;
import cz.pcejka.brickshop.repository.OrderOrderContentRepository;
import cz.pcejka.brickshop.repository.OrderRepository;
import cz.pcejka.brickshop.repository.OrdercontentRepository;
import cz.pcejka.brickshop.services.OrderService;
import cz.pcejka.brickshop.services.StreamService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ItemRepository itemRepository;
    private final OrdercontentRepository ordercontentRepository;
    private final OrderOrderContentRepository orderOrderContentRepository;
    private final DomainMapper domainMapper;
    private final StreamService streamService;

    @Override
    public Mono<Order> getOrder(UUID id) {
        return orderRepository.findById(id)
                .doOnNext(order -> log.info("For id '{}' found Order '{}'", id, order));
    }

    @Override
    public Flux<Order> getOrders() {
        return orderRepository.findAll()
                .doOnNext(order -> log.info("Found Order '{}'", order));
    }

    @Override
    public Mono<Order> createOrder(CreateOrderDto createOrderDTO) {
        var items = createOrderDTO.getItems();
        var itemsMappedStream = items.entrySet().stream().map(entry -> OrderContent.builder().itemId(entry.getKey()).quantity(entry.getValue()).build()).toList();
        var mapped = domainMapper.toDomain(createOrderDTO);
        return orderRepository.save(mapped)
                .flatMap(order -> ordercontentRepository.saveAll(itemsMappedStream)
                        .flatMap(orderContent -> orderOrderContentRepository.insert(new OrderOrderContent(order.getId(), orderContent.getId()))).then().thenReturn(order))
                .flatMap(order -> streamService.sendOrderEvent(order.getId(), domainMapper.toEvent(createOrderDTO)).thenReturn(order))
                .doOnSuccess(order -> log.info("Created Order with id '{}'", order.getId()));
    }

    @Override
    public Mono<cz.pcejka.brickshop.messaging.model.order.Order> processOrderStream(cz.pcejka.brickshop.messaging.model.order.Order order) {
        return Flux.fromIterable(order.getItems().entrySet())
                .flatMap(entry -> itemRepository.existsByIdAndQuantityGreaterThanEqual(entry.getKey(), entry.getValue().getKey())
                        .filter(inStock -> inStock)
                        .flatMap(unused -> itemRepository.findById(entry.getKey()))
                        .flatMap(item -> {
                            var newItem = item.toBuilder().quantity(item.getQuantity() - entry.getValue().getKey()).newlyCreated(false).build();
                            return itemRepository.save(newItem);
                        })
                        .map(unused -> {
                            entry.getValue().setValue(true);
                            return entry;
                        })
                        .switchIfEmpty(Mono.just(entry))
                )
                .collectMap(Map.Entry::getKey, Map.Entry::getValue)
                .map(entryMap -> order.toBuilder().items(entryMap).build());
    }

    @Override
    public Mono<Order> updateOrder(UUID id, UpdateOrderDto updateOrderDTO) {
        return orderRepository.findAll().collectList()
                .then(orderRepository.findById(id)
                        .switchIfEmpty(Mono.just(new Order()))
                        .flatMap(oldOrder -> orderRepository.save(domainMapper.updateOrder(updateOrderDTO, oldOrder)))
                        .flatMap(order -> streamService.sendOrderEvent(id, new UpdateOrderEvent(updateOrderDTO.getDescription())).thenReturn(order))
                        .doOnSuccess(order -> log.info("Order '{}' updated", order.getId())));
    }

    @Override
    public Mono<Void> deleteOrder(UUID id) {
        return orderRepository.deleteById(id)
                .then(streamService.sendOrderEvent(id, new DeleteOrderEvent()))
                .doOnSuccess(unused -> log.info("Order '{}' deleted", id));
    }

    @Override
    public Mono<Void> refreshOrder(UUID orderId) {
        return streamService.sendOrderEvaluation(orderId, new RefreshProcessingEvent());
    }

    @Override
    public Mono<UUID> updateOrderState(UUID orderId, Order.State state) {
        return orderRepository.findById(orderId)
                .map(order -> order.toBuilder().state(state).newlyCreated(false).build())
                .flatMap(orderRepository::save)
                .map(Order::getId);
    }

}
