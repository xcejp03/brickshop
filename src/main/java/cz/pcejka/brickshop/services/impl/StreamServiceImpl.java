package cz.pcejka.brickshop.services.impl;

import cz.pcejka.brickshop.messaging.model.Event;
import cz.pcejka.brickshop.services.StreamService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.UUID;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StreamServiceImpl implements StreamService {

    public static final String ORDER_EVENTS_BINDING = "order-events-out";

    private final StreamBridge streamBridge;

    @Override
    public Mono<Void> sendOrderEvent(UUID key, Event payload) {
        return Mono.defer(() -> {
            streamBridge.send(ORDER_EVENTS_BINDING, createKafkaMessage(key.toString(), payload));
            log.info("Order emitted: '{} - {} to {}'", key, payload, ORDER_EVENTS_BINDING);
            return Mono.empty();
        });
    }

    @Override
    public Mono<Void> sendOrderEvaluation(UUID key, Event payload) {
        return Mono.defer(() -> {
            streamBridge.send(ORDER_EVENTS_BINDING, createKafkaMessage(key.toString(), payload));
            log.info("Evaluation emitted: '{} - {}'", key, payload);
            return Mono.empty();
        });
    }

    private <T> Message<T> createKafkaMessage(String key, T value) {
        return MessageBuilder
                .withPayload(value)
                .setHeader(KafkaHeaders.MESSAGE_KEY, key)
                .build();
    }
}
