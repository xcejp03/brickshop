package cz.pcejka.brickshop.services.impl;

import cz.pcejka.brickshop.domain.User;
import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.model.CreateUserDto;
import cz.pcejka.brickshop.model.UpdateUserDto;
import cz.pcejka.brickshop.repository.UserRepository;
import cz.pcejka.brickshop.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final DomainMapper domainMapper;

    @Override
    public Mono<User> getUser(UUID id) {
        return userRepository.findById(id)
                .doOnNext(user -> log.info("For id '{}' found User '{}'", id, user));
    }

    @Override
    public Flux<User> getUsers() {
        return userRepository.findAll()
                .doOnNext(user -> log.info("Found User '{}'", user));
    }

    @Override
    public Mono<User> createUser(CreateUserDto createUserDTO) {
        return userRepository.save(domainMapper.toDomain(createUserDTO))
                .doOnSuccess(user -> log.info("Created User with id '{}'", user.getId()));
    }

    @Override
    public Mono<User> updateUser(UUID id, UpdateUserDto updateUserDTO) {
        return userRepository.findById(id).switchIfEmpty(Mono.just(new User()))
                .flatMap(oldUser -> userRepository.save(domainMapper.updateUser(updateUserDTO, oldUser)))
                .doOnSuccess(user -> log.info("User '{}' updated", user.getId()));
    }

    @Override
    public Mono<Void> delete(UUID id) {
        return userRepository.deleteById(id)
                .doOnSuccess(unused -> log.info("User '{}' deleted", id));
    }
}
