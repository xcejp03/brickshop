/*
 * Copyright (C) Lundegaard a.s. 2022 - All Rights Reserved
 *
 * Proprietary and confidential. Unauthorized copying of this file, via any
 * medium is strictly prohibited.
 */
package cz.pcejka.brickshop.services.impl;

import cz.pcejka.brickshop.domain.Order;
import cz.pcejka.brickshop.repository.OrderRepository;
import cz.pcejka.brickshop.services.MetricService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MetricServiceImpl implements MetricService {

    private final MeterRegistry meterRegistry;
    private final OrderRepository orderRepository;


    @PostConstruct
    void setup() {
        initOpenOrderGauge();
    }

    private void initOpenOrderGauge() {
        meterRegistry.gauge(OPEN_ORDER_GAUGE, orderRepository,
                repository -> Optional.ofNullable(repository.countAllByState(Order.State.OPEN).block()).orElse(0L));
    }


    @Override
    public void incrementClosedOrder() {
        Counter.builder(CLOSED_ORDER_COUNTER)
                .description("Closed order counter")
                .register(meterRegistry)
                .increment();
    }
}
