package cz.pcejka.brickshop.services.impl;

import cz.pcejka.brickshop.domain.Item;
import cz.pcejka.brickshop.mapping.DomainMapper;
import cz.pcejka.brickshop.messaging.model.evaluation.AbstractEvaluationEvent;
import cz.pcejka.brickshop.messaging.model.evaluation.WarehouseChangeEvent;
import cz.pcejka.brickshop.model.CreateItemDto;
import cz.pcejka.brickshop.model.UpdateItemDto;
import cz.pcejka.brickshop.repository.ItemRepository;
import cz.pcejka.brickshop.services.ItemService;
import cz.pcejka.brickshop.services.StreamService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;
    private final DomainMapper domainMapper;
    private final StreamService streamService;

    @Override
    public Mono<Item> getItem(UUID id) {
        return itemRepository.findById(id)
                .doOnNext(item -> log.info("For id '{}' found Item '{}'", id, item));
    }

    @Override
    public Flux<Item> getItems() {
        return itemRepository.findAll()
                .doOnNext(item -> log.info("Found Item '{}'", item));
    }

    @Override
    public Mono<Item> createItem(CreateItemDto createItemDto) {
        return itemRepository.save(domainMapper.toDomain(createItemDto))
                .doOnSuccess(item -> log.info("Created Item with id '{}'", item.getId()));
    }

    @Override
    public Mono<Item> updateItem(UUID id, UpdateItemDto updateItemDto) {
        return itemRepository.findById(id).switchIfEmpty(Mono.just(new Item()))
                .flatMap(oldItem -> itemRepository.save(domainMapper.updateItem(updateItemDto, oldItem)))
                .doOnSuccess(item -> log.info("Item '{}' updated", item.getId()));
    }

    @Override
    public Mono<Void> deleteItem(UUID id) {
        return itemRepository.deleteById(id)
                .doOnSuccess(unused -> log.info("Item '{}' deleted", id));
    }

    @Override
    public Mono<Void> storeItems(Map<UUID, Long> items) {
        return Flux.fromIterable(items.entrySet())
                .flatMap(entry -> itemRepository.findById(entry.getKey())
                        .switchIfEmpty(Mono.defer(() -> {
                            log.warn("Trying to store non existing item: '{}'", entry.getKey());
                            return Mono.empty();
                        }))
                        .map(item -> item.toBuilder().quantity(item.getQuantity() + entry.getValue()).newlyCreated(false).build())
                        .flatMap(itemRepository::save))
                .flatMap(item -> streamService.sendOrderEvaluation(UUID.randomUUID(),
                        WarehouseChangeEvent.builder().source(AbstractEvaluationEvent.EventSource.ITEMS_STOCKED)
                                .build()))
                .then();
    }

    @Override
    public Flux<Map.Entry<UUID, Long>> getItemsQuantity(List<UUID> items) {
        return itemRepository.findAllById(items)
                .map(item -> Map.entry(item.getId(), item.getQuantity()));
    }

    @Override
    public Mono<Void> setItemAmount(Map<UUID, Long> items) {
        return Flux.fromIterable(items.entrySet())
                .flatMap(entry -> itemRepository.findById(entry.getKey())
                        .switchIfEmpty(Mono.defer(() -> {
                            log.warn("Trying to store non existing item: '{}'",
                                    entry.getKey());
                            return Mono.empty();
                        }))
                        .map(item -> item.toBuilder().quantity(entry.getValue())
                                .newlyCreated(false).build())
                        .flatMap(itemRepository::save))
                .flatMap(item -> streamService.sendOrderEvaluation(UUID.randomUUID(),
                        WarehouseChangeEvent.builder().source(AbstractEvaluationEvent.EventSource.ITEMS_STOCKED)
                                .build()))
                .then();
    }

    @Override
    public Flux<Map.Entry<UUID, Long>> getAllItemsQuantity() {
        return itemRepository.findAll()
                .map(item -> Map.entry(item.getId(), item.getQuantity()));
    }
}
