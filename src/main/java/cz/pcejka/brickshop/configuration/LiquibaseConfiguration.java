package cz.pcejka.brickshop.configuration;

import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(LiquibaseProperties.class)
public class LiquibaseConfiguration {

    private final LiquibaseProperties liquibaseProperties;

    @Bean
    @LiquibaseDataSource
    public DataSource dataSource() {
        return new DriverManagerDataSource(
                liquibaseProperties.getUrl(),
                liquibaseProperties.getUser(),
                liquibaseProperties.getPassword()
        );
    }
}
