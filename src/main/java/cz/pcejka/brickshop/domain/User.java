package cz.pcejka.brickshop.domain;

import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.time.Instant;
import java.util.UUID;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

@EqualsAndHashCode(callSuper = false)
@Table("users")
@Data
@Builder(toBuilder = true)
@FieldNameConstants(asEnum = true)
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractEntity<UUID> {

    @NonNull
    @Id
    private UUID id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private Role role;

    @CreatedDate
    private Instant created;

    @LastModifiedDate
    private Instant updated;

}
