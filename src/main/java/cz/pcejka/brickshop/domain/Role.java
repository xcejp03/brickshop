package cz.pcejka.brickshop.domain;

public enum Role {

    CUSTOMER, ADMIN

}
