package cz.pcejka.brickshop.domain;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

import static cz.pcejka.brickshop.domain.OrderContent.TABLE_NAME;

@EqualsAndHashCode(callSuper = false)
@Table(TABLE_NAME)
@Data
@SuperBuilder(toBuilder = true)
@FieldNameConstants(asEnum = true)
@NoArgsConstructor
@AllArgsConstructor
public class OrderContent extends AbstractEntity<UUID> {

    public static final String TABLE_NAME = "ordercontent";

    @NonNull
    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    private UUID itemId;

    private Long quantity;

}
