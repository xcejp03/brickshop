package cz.pcejka.brickshop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.UUID;

import org.springframework.data.relational.core.mapping.Table;

import static cz.pcejka.brickshop.domain.OrderOrderContent.TABLE_NAME;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(TABLE_NAME)
public class OrderOrderContent {

    public static final String TABLE_NAME = "order_ordercontent";

    private UUID orderId;
    private UUID contentId;
}
