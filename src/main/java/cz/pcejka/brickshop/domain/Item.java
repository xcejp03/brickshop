package cz.pcejka.brickshop.domain;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

@EqualsAndHashCode(callSuper = false)
@Table("item")
@Data
@SuperBuilder(toBuilder = true)
@FieldNameConstants(asEnum = true)
@NoArgsConstructor
@AllArgsConstructor
public class Item extends AbstractEntity<UUID> {

    @NonNull
    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    private String name;

    private long quantity;

    private String description;

    private BigDecimal price;

    @LastModifiedDate
    private Instant updated;

}
