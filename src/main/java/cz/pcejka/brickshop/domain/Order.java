package cz.pcejka.brickshop.domain;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

@EqualsAndHashCode(callSuper = true)
@Table("orders")
@Data
@SuperBuilder(toBuilder = true)
@FieldNameConstants(asEnum = true)
@NoArgsConstructor
@AllArgsConstructor
public class Order extends AbstractEntity<UUID> {

    @NonNull
    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    private UUID customerId;

    private String description;

    @CreatedDate
    private Instant created;

    @LastModifiedDate
    private Instant updated;

    @Builder.Default
    private State state = State.OPEN;

    public enum State {
        OPEN, CLOSED
    }

}
