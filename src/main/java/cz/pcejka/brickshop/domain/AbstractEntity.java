package cz.pcejka.brickshop.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;

@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEntity<T> implements Persistable<T> {

    @Transient
    @Builder.Default
    private boolean newlyCreated = true;

    @JsonIgnore
    @Override
    public boolean isNew() {
        return newlyCreated;
    }
}
