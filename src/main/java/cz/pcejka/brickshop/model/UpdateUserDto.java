package cz.pcejka.brickshop.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class UpdateUserDto {

    private final String firstName;

    private final String lastName;

}
