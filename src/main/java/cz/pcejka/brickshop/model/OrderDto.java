package cz.pcejka.brickshop.model;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
@RequiredArgsConstructor
@Builder(toBuilder = true)
public class OrderDto {

    private final UUID id;

    private final List<UUID> items;

    private final UUID customerId;

    private final String description;

    private final Instant created;

    private final Instant updated;
}
