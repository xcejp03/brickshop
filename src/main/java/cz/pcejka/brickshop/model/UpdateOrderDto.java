package cz.pcejka.brickshop.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class UpdateOrderDto {

    private String description;

}
