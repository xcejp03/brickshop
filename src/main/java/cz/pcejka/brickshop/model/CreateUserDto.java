package cz.pcejka.brickshop.model;

import cz.pcejka.brickshop.domain.Role;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class CreateUserDto {

    private final String firstName;

    private final String lastName;

    private final String email;

    private final Role role;

    private final String password;

}
