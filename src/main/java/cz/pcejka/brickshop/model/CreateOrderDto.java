package cz.pcejka.brickshop.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

import java.util.Map;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderDto {

    @NotEmpty
    private Map<UUID, Long> items;

    private UUID customerId;

    private String description;

}
