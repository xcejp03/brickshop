package cz.pcejka.brickshop.model;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@RequiredArgsConstructor
@Builder(toBuilder = true)
public class ItemDto {

    private final UUID id;

    private final String name;

    private final String description;

    private final BigDecimal price;

}
