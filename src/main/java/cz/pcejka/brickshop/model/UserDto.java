package cz.pcejka.brickshop.model;

import cz.pcejka.brickshop.domain.Role;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
@Builder(toBuilder = true)
public class UserDto {

    private final UUID id;

    private final String firstName;

    private final String lastName;

    private final String email;

    private final Role role;

}
