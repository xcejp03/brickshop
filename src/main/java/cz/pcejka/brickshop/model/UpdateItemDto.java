package cz.pcejka.brickshop.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder(toBuilder = true)
public class UpdateItemDto {

    private final String name;

    private final String description;

    private final BigDecimal price;

}
