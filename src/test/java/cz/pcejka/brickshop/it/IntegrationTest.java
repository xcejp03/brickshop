package cz.pcejka.brickshop.it;

import cz.pcejka.brickshop.model.*;
import cz.pcejka.brickshop.repository.*;
import cz.pcejka.brickshop.services.StreamService;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static cz.pcejka.brickshop.controller.ItemController.ITEMS;
import static cz.pcejka.brickshop.controller.OrderController.ORDERS;
import static cz.pcejka.brickshop.controller.WarehouseController.WAREHOUSE;

@ActiveProfiles("test")
@AutoConfigureWebTestClient(timeout = "PT5M")
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "spring.kafka.bootstrap-servers= ${spring.embedded.kafka.brokers}"
        })
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {"log.dir=${kafka.test.logdir}/kafka--test-${random.uuid}"},
        topics = {"order-events"}
)
public class IntegrationTest {

    @Autowired
    protected WebTestClient client;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected ItemRepository itemRepository;

    @Autowired
    protected OrderRepository orderRepository;

    @Autowired
    protected OrdercontentRepository ordercontentRepository;

    @Autowired
    protected OrderOrderContentRepository orderOrderContentRepository;

    @Autowired
    protected StreamService streamService;

    @Autowired
    protected EmbeddedKafkaBroker kafkaBroker;

    @AfterEach
    public void tearDown() {
        userRepository.deleteAll().block(Duration.ofSeconds(1L));
        itemRepository.deleteAll().block(Duration.ofSeconds(1L));
        orderRepository.deleteAll().block(Duration.ofSeconds(1L));
        ordercontentRepository.deleteAll().block(Duration.ofSeconds(1L));
        orderOrderContentRepository.deleteAll().block(Duration.ofSeconds(1L));

    }

    protected OrderDto createOrder(CreateOrderDto orderDTO) {
        return client.post()
                .uri(ORDERS)
                .bodyValue(orderDTO)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(OrderDto.class)
                .returnResult().getResponseBody();
    }

    protected List<Map<UUID, Long>> getItemsQuantity(List<UUID> ids) {
        return client.post()
                .uri(WAREHOUSE + "/quantity")
                .bodyValue(ids)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(new ParameterizedTypeReference<Map<UUID, Long>>() {
                })
                .returnResult().getResponseBody();
    }

    protected ItemDto createItems(CreateItemDto createItemDTO) {
        return client.post()
                .uri(ITEMS)
                .bodyValue(createItemDTO)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(ItemDto.class)
                .returnResult().getResponseBody();
    }

    protected void storeItems(Map<UUID, Long> items) {
        client.post()
                .uri(WAREHOUSE)
                .bodyValue(items)
                .exchange()
                .expectStatus()
                .isNoContent();
    }

    protected OrderDto updateOrder(UUID orderId, UpdateOrderDto orderDTO) {
        return client.put()
                .uri(ORDERS + "/{orderId}", orderId)
                .bodyValue(orderDTO)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(OrderDto.class)
                .returnResult().getResponseBody();
    }

    protected void deleteOrder(UUID orderId) {
        client.delete()
                .uri(ORDERS + "/{orderId}", orderId)
                .exchange()
                .expectStatus()
                .isNoContent();
    }

    protected void refreshOrder(UUID orderId) {
        client.post()
                .uri(ORDERS + "/{orderId}/refresh", orderId)
                .exchange()
                .expectStatus()
                .isNoContent();
    }

}
