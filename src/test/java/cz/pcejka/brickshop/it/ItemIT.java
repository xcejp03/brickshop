package cz.pcejka.brickshop.it;

import cz.pcejka.brickshop.model.CreateItemDto;
import cz.pcejka.brickshop.model.ItemDto;
import cz.pcejka.brickshop.model.UpdateItemDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.pcejka.brickshop.controller.ItemController.ITEMS;
import static org.assertj.core.api.Assertions.assertThat;

class ItemIT extends IntegrationTest {

    @Test
    void CRUDItem() {
        //Get all Items - return empty list
        client.get()
                .uri(ITEMS)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(ItemDto.class)
                .hasSize(0);

        var createItem = CreateItemDto.builder()
                .name("Hammer")
                .description("without nails")
                .build();

        //Create Item
        var createdItem = client.post()
                .uri(ITEMS)
                .bodyValue(createItem)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(ItemDto.class)
                .returnResult().getResponseBody();

        //Get all items
        client.get()
                .uri(ITEMS)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(ItemDto.class)
                .value(itemDTOS -> assertThat(itemDTOS).usingRecursiveComparison().isEqualTo(List.of(createdItem)));

        //Update item

        var update = UpdateItemDto.builder()
                .name("changed").build();
        var updated = client
                .put()
                .uri(ITEMS + "/" + createdItem.getId())
                .bodyValue(update)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(ItemDto.class)
                .returnResult().getResponseBody();

        //Get updated item
        client.get()
                .uri(ITEMS + "/" + createdItem.getId())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(ItemDto.class)
                .value(itemDTO -> assertThat(itemDTO).usingRecursiveComparison().isEqualTo(updated));

        //Delete item
        client.delete()
                .uri(ITEMS + "/" + createdItem.getId())
                .exchange()
                .expectStatus()
                .isNoContent();

        //Get deleted item
        client.get()
                .uri(ITEMS + "/" + createdItem.getId())
                .exchange()
                .expectStatus()
                .isNotFound();
    }

}
