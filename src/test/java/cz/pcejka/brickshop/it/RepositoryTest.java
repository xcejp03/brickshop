package cz.pcejka.brickshop.it;

import cz.pcejka.brickshop.domain.*;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

class RepositoryTest extends IntegrationTest {

    @Test
    void userTest() {
        User user = new User(UUID.randomUUID(), "name", "lastName", "user@test.cz", "password1234", Role.ADMIN, Instant.now(), Instant.now());

        StepVerifier.create(userRepository.save(user))
                .expectNextMatches(actual -> Matchers.samePropertyValuesAs(user, "created", "updated").matches(actual))
                .verifyComplete();

        StepVerifier.create(userRepository.findAll())
                .expectNextMatches(actual -> Matchers.samePropertyValuesAs(user, "created", "updated").matches(actual))
                .verifyComplete();

        StepVerifier.create(userRepository.findById(user.getId()))
                .expectNextMatches(actual -> Matchers.samePropertyValuesAs(user, "created", "updated").matches(actual))
                .verifyComplete();
    }

    @Test
    void itemTest() {
        Item item = new Item(UUID.randomUUID(), "name", 128L, "description", BigDecimal.TEN, Instant.now());

        StepVerifier.create(itemRepository.save(item))
                .expectNext(item)
                .verifyComplete();

        StepVerifier.create(itemRepository.findAll())
                .expectNextMatches(actual -> Matchers.samePropertyValuesAs(item, "updated").matches(actual))
                .verifyComplete();

        StepVerifier.create(itemRepository.findById(item.getId()))
                .expectNextMatches(actual -> Matchers.samePropertyValuesAs(item, "updated").matches(actual))
                .verifyComplete();
    }

    @Test
    void orderTest() {
        Order order = Order.builder().description("Order description").customerId(UUID.randomUUID()).build();

        StepVerifier.create(orderRepository.save(order))
                .expectNext(order)
                .verifyComplete();

        StepVerifier.create(orderRepository.findAll())
                .expectNextMatches(actual -> Matchers.samePropertyValuesAs(order, "created", "updated").matches(actual))
                .verifyComplete();

        StepVerifier.create(orderRepository.findById(order.getId()))
                .expectNextMatches(actual -> Matchers.samePropertyValuesAs(order, "created", "updated").matches(actual))
                .verifyComplete();
    }

    @Test
    void cascadeDelete() {
        Order order = Order.builder()
                .description("For garage")
                .build();

        StepVerifier.create(orderRepository.save(order))
                .expectNext(order)
                .verifyComplete();

        Item itemBrick = Item.builder()
                .quantity(1000L)
                .description("Red brick")
                .name("brick")
                .price(BigDecimal.valueOf(4.99))
                .build();

        StepVerifier.create(itemRepository.save(itemBrick))
                .expectNext(itemBrick)
                .verifyComplete();

        Item itemCement = Item.builder()
                .quantity(34L)
                .description("The strongest cement")
                .name("cement")
                .price(BigDecimal.valueOf(24.99))
                .build();

        StepVerifier.create(itemRepository.save(itemCement))
                .expectNext(itemCement)
                .verifyComplete();

        OrderContent orderContent1 = OrderContent.builder()
                .quantity(100L)
                .itemId(itemBrick.getId())
                .build();

        StepVerifier.create(ordercontentRepository.save(orderContent1))
                .expectNext(orderContent1)
                .verifyComplete();

        OrderContent orderContent2 = OrderContent.builder()
                .quantity(2L)
                .itemId(itemCement.getId())
                .build();

        StepVerifier.create(ordercontentRepository.save(orderContent2))
                .expectNext(orderContent2)
                .verifyComplete();

        StepVerifier.create(orderOrderContentRepository.insert(new OrderOrderContent(order.getId(), orderContent1.getId())))
                .verifyComplete();

        StepVerifier.create(orderOrderContentRepository.insert(new OrderOrderContent(order.getId(), orderContent2.getId())))
                .verifyComplete();

        StepVerifier.create(orderOrderContentRepository.findAll())
                .expectNextCount(2L)
                .verifyComplete();

        StepVerifier.create(orderRepository.delete(order)).verifyComplete();

        StepVerifier.create(orderOrderContentRepository.findAll())
                .verifyComplete();
    }

}
