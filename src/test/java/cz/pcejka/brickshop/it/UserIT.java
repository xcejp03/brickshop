package cz.pcejka.brickshop.it;

import cz.pcejka.brickshop.domain.Role;
import cz.pcejka.brickshop.model.CreateUserDto;
import cz.pcejka.brickshop.model.UpdateUserDto;
import cz.pcejka.brickshop.model.UserDto;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.pcejka.brickshop.controller.UserController.USERS;
import static org.assertj.core.api.Assertions.assertThat;

class UserIT extends IntegrationTest {

    @Test
    void CRUDUser() {
        // There is no user - return empty list
        client.get()
                .uri(USERS)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(UserDto.class)
                .hasSize(0);

        //Create user
        var userBugsBunny = CreateUserDto.builder()
                .email("BugsBunny@acme.com")
                .firstName("Bugs")
                .lastName("Bunny")
                .password("Test1234")
                .role(Role.ADMIN)
                .build();

        var createdUser =
                client.post()
                        .uri(USERS)
                        .bodyValue(userBugsBunny)
                        .exchange()
                        .expectStatus()
                        .isOk()
                        .expectBody(UserDto.class)
                        .returnResult()
                        .getResponseBody();

        client.get()
                .uri(USERS)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(UserDto.class)
                .value(userDTOS -> assertThat(userDTOS).usingRecursiveComparison().isEqualTo(List.of(createdUser)));

        //Update user
        var update = UpdateUserDto.builder()
                .firstName("updatedFirst")
                .lastName("updatedSecond")
                .build();

        var updatedUser = client.put()
                .uri(USERS + "/" + createdUser.getId())
                .bodyValue(update)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(UserDto.class)
                .returnResult().getResponseBody();

        //Get updated item
        client.get()
                .uri(USERS)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(UserDto.class)
                .value(userDTOS -> assertThat(userDTOS).usingRecursiveComparison().isEqualTo(List.of(updatedUser)));

        //Delete user
        client.delete()
                .uri(USERS + "/" + createdUser.getId())
                .exchange()
                .expectStatus()
                .isNoContent();

        //Get deleted item
        client.get()
                .uri(USERS + "/" + createdUser.getId())
                .exchange()
                .expectStatus()
                .isNotFound();
    }
}
