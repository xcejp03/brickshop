package cz.pcejka.brickshop.it;

import cz.pcejka.brickshop.domain.Item;
import cz.pcejka.brickshop.messaging.model.evaluation.AbstractEvaluationEvent;
import cz.pcejka.brickshop.model.CreateItemDto;
import cz.pcejka.brickshop.model.CreateOrderDto;
import cz.pcejka.brickshop.model.OrderDto;
import cz.pcejka.brickshop.model.UpdateOrderDto;
import cz.pcejka.brickshop.utils.TestingConsumer;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.stream.test.binder.TestSupportBinderAutoConfiguration;

import static cz.pcejka.brickshop.controller.OrderController.ORDERS;
import static cz.pcejka.brickshop.messaging.model.evaluation.AbstractEvaluationEvent.EventSource.*;
import static cz.pcejka.brickshop.messaging.model.evaluation.ProcessOrderEvent.PROCESS_TYPE;
import static cz.pcejka.brickshop.messaging.model.evaluation.RefreshProcessingEvent.REFRESH_TYPE;
import static cz.pcejka.brickshop.messaging.model.evaluation.WarehouseChangeEvent.WAREHOUSE_TYPE;
import static cz.pcejka.brickshop.messaging.model.order.CreateOrderEvent.CREATE_TYPE;
import static cz.pcejka.brickshop.messaging.model.order.DeleteOrderEvent.DELETE_TYPE;
import static org.assertj.core.api.Assertions.assertThat;

@EnableAutoConfiguration(exclude = TestSupportBinderAutoConfiguration.class)
class OrderIT extends IntegrationTest {

    private static final String ORDER_EVENT = """
            {"type":"%s","items":{%s},"customerId":null,"description":"%s"}""";
    private static final String EVALUATION_EVENT = """
            {"type":"%s","source":"%s"}""";
    private static final String FULFILLED_EVENT = """
            {"type":"FULFILLED","orders":[%s]}""";
    private static final String TYPE_EVENT = """
            {"type":"%s"}""";
    private static final String UPDATE_EVENT = """
            {"type":"UPDATE","description":"%s"}""";
    private static final String ITEMS = """
            "%s":%s""";

    @Test
    void createOrder() {
        TestingConsumer testingConsumer = new TestingConsumer(kafkaBroker);
        Map<UUID, Long> items = new HashMap<>();
        var itemId = UUID.randomUUID();
        items.put(itemId, 3L);

        var createOrder = CreateOrderDto.builder()
                .items(items)
                .description("Don't forget me")
                .build();
        createOrder(createOrder);

        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 3L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));
    }

    @Test
    void refreshCreatedOrder() {
        TestingConsumer testingConsumer = new TestingConsumer(kafkaBroker);
        Map<UUID, Long> items = new HashMap<>();
        var itemId = UUID.randomUUID();
        items.put(itemId, 3L);
        var createOrder = CreateOrderDto.builder()
                .description("Don't forget me")
                .items(items)
                .build();
        var createdOrder = createOrder(createOrder);

        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 3L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));

        //refresh
        refreshOrder(createdOrder.getId());
        testingConsumer.pollAndAssert(TYPE_EVENT.formatted(REFRESH_TYPE));
    }

    @Test
    void createOrderAndThenWarehouseChange() {
        TestingConsumer testingConsumer = new TestingConsumer(kafkaBroker);
        var itemId = createItems(CreateItemDto.builder().name("Brick").price(BigDecimal.valueOf(1.99)).build()).getId();
        assertThat(getItemsQuantity(List.of(itemId))).hasSize(1);
        Map<UUID, Long> items = new HashMap<>();
        items.put(itemId, 23L);
        var createOrder = CreateOrderDto.builder()
                .description("Don't forget me")
                .items(items)
                .build();
        var orderId = createOrder(createOrder).getId();
        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 23L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));
        // order is created

        //refresh order
        refreshOrder(orderId);
        testingConsumer.pollAndAssert(TYPE_EVENT.formatted(REFRESH_TYPE));

        assertThat(getItemsQuantity(List.of(itemId)).get(0).get(itemId)).isZero();
        //put items on stock
        storeItems(Map.of(itemId, 50L));
        assertThat(getItemsQuantity(List.of(itemId)).get(0).get(itemId)).isEqualTo(50L);

        //items are removed from warehouse
        testingConsumer.pollAndAssert(EVALUATION_EVENT.formatted(WAREHOUSE_TYPE, AbstractEvaluationEvent.EventSource.ITEMS_STOCKED), FULFILLED_EVENT.formatted(quoted(orderId.toString())));
        assertThat(getItemsQuantity(List.of(itemId)).get(0).get(itemId)).isEqualTo(27L);
    }

    @Test
    void fulfillOrdersAfterStoringItems() {
        TestingConsumer testingConsumer = new TestingConsumer(kafkaBroker);
        Map<UUID, Long> items = new HashMap<>();
        var itemId = UUID.randomUUID();
        items.put(itemId, 4L);
        var createOrder = CreateOrderDto.builder()
                .description("Don't forget me")
                .items(items)
                .build();
        var orderId3 = createOrder(createOrder).getId();
        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 4L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));
        var orderId2 = createOrder(createOrder).getId();
        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 4L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));
        var orderId1 = createOrder(createOrder).getId();
        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 4L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));
        //orders are created

        itemRepository.save(Item.builder().id(itemId).quantity(1L).build()).block();
        assertThat(getItemsQuantity(List.of(itemId)).get(0).get(itemId)).isEqualTo(1L);
        refreshOrder(orderId3);
        testingConsumer.pollAndAssert(TYPE_EVENT.formatted(REFRESH_TYPE));
        itemRepository.save(Item.builder().id(itemId).quantity(4L).newlyCreated(false).build()).block();
        assertThat(getItemsQuantity(List.of(itemId)).get(0).get(itemId)).isEqualTo(4L);
        refreshOrder(orderId3);
        testingConsumer.pollAndAssert(TYPE_EVENT.formatted(REFRESH_TYPE), FULFILLED_EVENT.formatted(quoted(orderId3.toString())));
        assertThat(getItemsQuantity(List.of(itemId)).get(0).get(itemId)).isZero();

        storeItems(Map.of(itemId, 10L));
        testingConsumer.pollAndAssert(s -> Matchers.anyOf(Matchers.contains(FULFILLED_EVENT.formatted(quoted(orderId2.toString()) + "," + quoted(orderId1.toString()))),
                        Matchers.contains(FULFILLED_EVENT.formatted(quoted(orderId1.toString()) + "," + quoted(orderId2.toString())))).matches(s),
                s -> Matchers.contains(EVALUATION_EVENT.formatted(WAREHOUSE_TYPE, AbstractEvaluationEvent.EventSource.ITEMS_STOCKED)).matches(s));
        assertThat(getItemsQuantity(List.of(itemId)).get(0).get(itemId)).isEqualTo(2L);
    }

    @Test
    void updateOrderWithEmit() {
        TestingConsumer testingConsumer = new TestingConsumer(kafkaBroker);
        Map<UUID, Long> items = new HashMap<>();
        var itemId = UUID.randomUUID();
        items.put(itemId, 7L);
        var createOrder = CreateOrderDto.builder()
                .description("VIP customer")
                .items(items)
                .build();
        var createdOrder = createOrder(createOrder);
        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 7L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));

        updateOrder(createdOrder.getId(), UpdateOrderDto.builder().description("Naah, just regular customer").build());

        testingConsumer.pollAndAssert(UPDATE_EVENT.formatted("Naah, just regular customer"), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_UPDATE));
    }

    @Test
    void deleteOrderWithEmit() {
        TestingConsumer testingConsumer = new TestingConsumer(kafkaBroker);
        var itemId = UUID.randomUUID();
        var createOrder = CreateOrderDto.builder()
                .description("For house")
                .items(Map.of(itemId, 9L))
                .build();
        var createdOrder = createOrder(createOrder);
        testingConsumer.pollAndAssert(ORDER_EVENT.formatted(CREATE_TYPE, ITEMS.formatted(itemId, 9L), createOrder.getDescription()), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_CREATE));

        deleteOrder(createdOrder.getId());

        testingConsumer.pollAndAssert(TYPE_EVENT.formatted(DELETE_TYPE), EVALUATION_EVENT.formatted(PROCESS_TYPE, ORDER_DELETE));
    }

    @Test
    void OrderCRUD() {
        //Get all Orders - return empty list
        client.get()
                .uri(ORDERS)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(OrderDto.class)
                .hasSize(0);

        var itemId = UUID.randomUUID();
        var createOrder = CreateOrderDto.builder()
                .items(Map.of(itemId, 9L))
                .description("rjrjeiei")
                .build();

        //Create Order
        var createdOrder = client.post()
                .uri(ORDERS)
                .bodyValue(createOrder)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(OrderDto.class)
                .returnResult().getResponseBody();

        //Get order
        client.get()
                .uri(ORDERS + "/" + createdOrder.getId())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(OrderDto.class)
                .value(orderDTO -> assertThat(orderDTO).usingRecursiveComparison().ignoringFields("created", "updated").isEqualTo(createdOrder));

        //Update order
        var update = UpdateOrderDto.builder()
                .description("upupip").build();
        var updated = client
                .put()
                .uri(ORDERS + "/" + createdOrder.getId())
                .bodyValue(update)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(OrderDto.class)
                .returnResult().getResponseBody();

        //Get updated order
        client.get()
                .uri(ORDERS + "/" + createdOrder.getId())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(OrderDto.class)
                .value(itemDTO -> assertThat(itemDTO).usingRecursiveComparison().ignoringFields("created", "updated").isEqualTo(updated));

        //Delete item
        client.delete()
                .uri(ORDERS + "/" + createdOrder.getId())
                .exchange()
                .expectStatus()
                .isNoContent();

        //Get deleted item
        client.get()
                .uri(ORDERS + "/" + createdOrder.getId())
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void missingItemsInOrderCreate() {
        var createOrder = CreateOrderDto.builder()
                .description("rjrjeiei")
                .build();

        client.post()
                .uri(ORDERS)
                .bodyValue(createOrder)
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    private String quoted(String txt) {
        String quote = "\"";
        return quote + txt + quote;
    }
}
