package cz.pcejka.brickshop.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.awaitility.Awaitility;

import java.time.Duration;
import java.util.*;
import java.util.function.Predicate;

import org.springframework.kafka.test.EmbeddedKafkaBroker;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class TestingConsumer {

    private final EmbeddedKafkaBroker kafkaBroker;
    private final KafkaConsumer<String, String> consumer;

    public TestingConsumer(EmbeddedKafkaBroker kafkaBroker) {
        this.kafkaBroker = kafkaBroker;
        this.consumer = createTestingConsumer();
    }

    public KafkaConsumer<String, String> createTestingConsumer() {
        String topic = "order-events";

        //Creating consumer properties
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBroker.getBrokersAsString());
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "testingConsumer-" + UUID.randomUUID());
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        //creating consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);

        //Subscribing
        kafkaBroker.consumeFromAnEmbeddedTopic(consumer, topic);
        consumer.seekToEnd(Collections.emptyList());
        consumer.assignment().forEach(consumer::position);
        return consumer;
    }

    public void pollAndAssert(Predicate<List<String>>... predicate) {
        log.info("POLLING >>>>>>>>>>>");
        List<Predicate<List<String>>> expected = new ArrayList<>(List.of(predicate));
        List<String> actual = new ArrayList<>();
        List<String> asserted = new ArrayList<>();

        Awaitility.await()
                .atMost(ofSeconds(3L))
                .pollDelay(ofMillis(100L))
                .pollInterval(ofMillis(150L))
                .untilAsserted(() -> {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(250));
                    log.info("Records count: {}", records.count());
                    records.forEach(stringStringConsumerRecord -> actual.add(stringStringConsumerRecord.value()));
                    log.info("Expected records: {}", expected.size());
                    log.info("Actual records : {} - {}", actual.size(), actual);
                    actual.forEach(s -> {
                        expected.stream().filter(listConsumer -> listConsumer.test(actual)).findFirst().ifPresent(expected::remove);
                        asserted.add(s);
                    });
                    actual.removeAll(asserted);
                    log.debug("Not asserted actual : {}", actual);
                    log.debug("Not asserted expected : {}", expected);
                    assertThat(expected).as("Not all assertions were completed").isEmpty();
                    assertThat(actual).as("Not all records were asserted").isEmpty();
                });
    }

    public void pollAndAssert(String... msg) {
        log.info("POLLING >>>>>>>>>>>");
        List<String> expected = new ArrayList<>(List.of(msg));
        List<String> actual = new ArrayList<>();
        Awaitility.await()
                .atMost(ofSeconds(3L))
                .pollDelay(ofMillis(100L))
                .pollInterval(ofMillis(150L))
                .untilAsserted(() -> {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(250));
                    log.info("Records count: {}", records.count());
                    records.forEach(stringStringConsumerRecord -> actual.add(stringStringConsumerRecord.value()));
                    log.info("Expected records: {} - {}", expected.size(), expected);
                    log.info("Actual records : {} - {}", actual.size(), actual);
                    List<String> asserted = new ArrayList<>();
                    actual.forEach(s -> {
                        assertThat(expected).contains(s);
                        expected.remove(s);
                        asserted.add(s);
                    });
                    actual.removeAll(asserted);
                    log.debug("Not asserted actual : {}", actual);
                    log.debug("Not asserted expected : {}", expected);
                    assertThat(expected).as("Not all assertions were completed").isEmpty();
                    assertThat(actual).as("Not all records were asserted").isEmpty();
                });
    }
}
